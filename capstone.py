from abc import ABC, abstractmethod

class Person(ABC):
    @abstractmethod
    def getFullName(self):
        pass

    @abstractmethod
    def addRequest(self):
        pass

    @abstractmethod
    def checkRequest(self):
        pass

    @abstractmethod
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    # Getters and Setters for private properties
    def getFirstName(self):
        return self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        return self._email

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return self._department

    def setDepartment(self, department):
        self._department = department

    # Abstract methods
    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def addRequest(self):
        return "Request has been added"

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    # Custom methods
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"
    
class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []

    # Getters and Setters for private properties
    def getFirstName(self):
        return self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        return self._email

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return self._department

    def setDepartment(self, department):
        self._department = department

    def get_members(self):
        return self._members

    def setMembers(self, members):
        self._members = members

    # Abstract methods
    def addRequest(self):
        pass

    def addUser(self):
        pass

    def checkRequest(self):
        return "Request has been checked"

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    # Custom methods
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def addMember(self, employee):
        self._members.append(employee)


class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    # Getters and Setters for private properties
    def getFirstName(self):
        return self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName

    def getLastName(self):
        return self._lastName

    def setLastName(self, lastName):
        self._lastName = lastName

    def getEmail(self):
        return self._email

    def setEmail(self, email):
        self._email = email

    def getDepartment(self):
        return self._department

    def setDepartment(self, department):
        self._department = department

    # Abstract methods
    def checkRequest(self):
        pass

    def addRequest(self):
        pass

    def addUser(self):
        return "User has been added"

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    # Custom methods
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"
class Request:
    def __init__(self, name, requester, dateRequested):
        self.__name = name
        self.__requester = requester
        self.__dateRequested = dateRequested
        self.__status = "Pending"
        
    # Getters and setters for the properties
    def getName(self):
        return self.__name
    
    def setName(self, name):
        self.__name = name
    
    def getRequester(self):
        return self.__requester
    
    def setRequester(self, requester):
        self.__requester = requester
    
    def getDateRequested(self):
        return self.__dateRequested
    
    def setDateRequested(self, dateRequested):
        self.__dateRequested = dateRequested
    
    def getStatus(self):
        return self.__status
    
    def set_status(self, status):
        self.__status = status
    
    # Other methods
    def updateRequest(self):
        return f"Request {self.__name} has been updated"
    
    def closeRequest(self):
        return f"Request {self.__name} has been closed"
    
    def cancelRequest(self):
        return f"Request {self.__name} has been cancelled"
    
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())


